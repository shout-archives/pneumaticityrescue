<?php

require_once("setup.php");
require_once("libs/email.php");

$errors = array();

if(empty($_POST['your_email']) || !filter_var($_POST['your_email'], FILTER_VALIDATE_EMAIL))
	$errors[] = 'Your email address is invalid';

if(empty($_POST['friends_email']) || !filter_var($_POST['friends_email'], FILTER_VALIDATE_EMAIL))
	$errors[] = 'Your friends email is invalid';

if(empty($_POST['your_message']))
	$errors[] = 'Please provide a message';

if(empty($errors))
{
	$email = new Email();
	$email->add_to($_POST['friends_email']);
	$email->set_subject('Pet Rescue Fundraiser');
	$email->set_from('noreply@helpapet.ca');
	$email->set_reply($_POST['your_email']);
	$email->set_body($_POST['your_message']);
	$email->send();

	echo json_encode(array(
		'status' => 'ok'
	));
}
else
{
	echo json_encode(array(
		'status' => 'error',
		'errors' => $errors
	));
}