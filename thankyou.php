<?php

require_once("setup.php");
require_once('stripe-php/init.php');

$errors = array();

$total_transactions = db_query("SELECT SUM(`amount`) AS `total` FROM `transactions`");
$total_transactions = $total_transactions->fetchObject();

if(empty($total_transactions->total))
	$total_transactions->total = 0;

include("theme/header.php"); ?>

	<div id="header">

		<img src="links/logo.png" alt="Pneumaticity - Pet Rescue Fundraiser">
		<img src="links/pets.jpg" alt="Help the animals">

		<div class="progressBar">
			<p class="headerText">Thank you for helping us raise funds for Kitty Kare, Northern Lights Dog Rescue and New Hope Dog Rescue.</p>
			<h2 class="barText">Amount raised to date:</h2>

			<div class="progress_bar">
				<div class="goal">$<?php echo number_format($goal_amount); ?></div>
				<div class="bar<?php if($total_transactions->total / $goal_amount * 100 < 8) echo ' outside'; ?><?php if($total_transactions->total > 10) echo ' double'; ?>" style="width: <?php

					if($total_transactions->total >= $goal_amount)
						echo 100;
					else
						echo $total_transactions->total / $goal_amount * 100;

					?>%">
					<div class="current_amount"><?php

					if($total_transactions->total >= $goal_amount)
						echo 'Goal Achieved!';
					else
						echo '$' . $total_transactions->total;

					?></div>
				</div>
			</div>

			<div class="help">
				<h2 class="barText">Help spread the word:</h2>
				<a href="#" class="button js-facebook-share">Share on your wall</a>
				<a href="#" class="button js-email-friend">Email a friend</a>
			</div>
		</div>

		<div class="spacer"></div>

	</div>

<?php include("theme/footer.php"); ?>