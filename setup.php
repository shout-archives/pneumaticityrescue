<?php

$config['app_id']     = '1684005668495996';
$config['app_secret'] = '';

$config['site_url']     = 'http://mobiletabs.ca/'; // Include trailing slash
$config['facebook_url'] = '';

$config['db']['host'] = 'localhost';
$config['db']['name'] = 'smcdncom_pneumaticity';
$config['db']['user'] = 'smcdncom_pmatici';
$config['db']['pass'] = '[d$)MeHHT(tw';

// $config['stripe']['secret'] = 'sk_test_YxdZDXt25o95L3e7IpSw1zwP';
// $config['stripe']['publish'] = 'pk_test_h0UDj6VXvdP66BqouDeF29o6';
$config['stripe']['secret'] = 'sk_live_oxRrg6qythvcO68nvX6Ru3Wl';
$config['stripe']['publish'] = 'pk_live_mjGMR6InvLAk2RbVIRhjh3ry';

$config['debug'] = true;

$goal_amount = 1000;

/***********************
 * Setup
 * Do not edit below here
 ***********************/

// Fix IE Session + Cookies within IFrames
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

// Activate Sessions
@session_start();

// Display PHP Errors
if($config['debug'])
	error_reporting(E_ALL ^ E_NOTICE);
else
	error_reporting(0);

// Connect to DB Where applicable
if(!empty($config['db']['name']) && !empty($config['db']['user']))
{
	$db = new PDO("mysql:host=" . $config['db']['host'] . ";dbname=" . $config['db']['name'], $config['db']['user'], $config['db']['pass']);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
	$db->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_TO_STRING);
}

// User Agent sniffing for Mobile Detection (Not Ideal)
function detect_mobile()
{
	$mobile = false;

	$useragent = $_SERVER['HTTP_USER_AGENT'];

	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent,0,4)))
	{
		$mobile = true;
	}

	return $mobile;
}

// Check if within Facebook tab
function detect_facebook()
{
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'facebook') !== false)
		return true;

	return false;
}

// Valid Email Check
function test_email_address($email)
{
	$email = str_replace(" ", "_", $email);

	$email_pattern = '/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/';

	if(!empty($email) && preg_match($email_pattern, $email))
		return true;

	return false;
}

// Process database interactions
function db_query($sql)
{
	global $db;

	$query = $db->prepare($sql);
	$args = func_get_args();
	array_shift($args);

	if(isset($args[0]) && is_array($args[0])) $args = $args[0];

	$query->execute($args);

	return $query;
}

/**
 * Return Useable Site URL
 *
 * @param string $extend Page/Asset to append
 * @return string Full site URL
 */
function site_url($extend = '')
{
	global $config;

	if(!defined('SITE_URL'))
	{
		if(!empty($config['site_url']) && $config['site_url'] != 'auto')
		{
			$url = $config['site_url'];

			if(substr($config['site_url'], -1) == "/")
				$url = substr($config['site_url'], 0, -1);
		}
		else
		{
			$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off" ? "https" : "http";
			$url = $protocol . "://" . $_SERVER['HTTP_HOST'];

			$valid_ip = "/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/i";

			if(strpos($_SERVER['HTTP_HOST'], ':') !== false)
				$host = substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'], ':'));
			else
				$host = $_SERVER['HTTP_HOST'];

			if(preg_match($valid_ip, $host) || strpos($host, 'localhost') !== false)
			{
				if(strpos($_SERVER['REQUEST_URI'], '/', 1) !== false)
					$url .= substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '/', 1));
				else
					$url .= $_SERVER['REQUEST_URI'];
			}
		}

		$url .= '/';

		define('SITE_URL', $url);
	}
	else
	{
		$url = SITE_URL;
	}

	if(substr($extend, 0, 1) == "/")
		$url .= substr($extend, 1);
	else
		$url .= $extend;

	return $url;
}