$(function() {
	$(".js-connect-facebook").click(function() {
		if(!$(this).hasClass('complete'))
		{
			FB.login(function(response) {
				if(response.status != 'not_authorized')
				{
					FB.api('/me', function(response) {
						$(".js-connect-facebook").addClass('complete').text('Facebook Connected!')

						if($(".js-facebook-first").val().length == 0)
							$(".js-facebook-first").val(response.first_name)
						if($(".js-facebook-last").val().length == 0)
							$(".js-facebook-last").val(response.last_name)
						if($(".js-facebook-email").val().length == 0)
							$(".js-facebook-email").val(response.email)
					});
				}
			},{
				scope: 'email'
			});
		}

		return false
	})

	$(".js-facebook-share").click(function() {
		FB.ui({
			method: 'feed',
			name: 'Pneumaticity - Pet Rescue Fundraiser',
			link: 'http://localhost/FacebookContests/Pneumaticity_Rescue/check.php',
			picture: '',
			caption: 'Help us raise funds for Kitty Kare, Northern Lights Dog Rescue and New Hope Dog Rescue.',
			description: "These organizations are committed to helping animals in need. With your help we want to help raise money for these three animal rescue organizations because every pet deserves a loving home."
		});

		return false;
	})

	$(".js-email-friend").click(function() {
		$(".email_dialog").show()
		$(".email_dialog").find('.error_container').hide()
		$(".email_dialog").find('input, textarea').val('')
		return false;
	})

	$(".js-cancel-email").click(function() {
		$(".email_dialog").hide()
		$(".email_dialog").find('.error_container').hide()
		$(".email_dialog").find('input, textarea').val('')
		return false;
	})

	// $(".email_dialog").click(function() {
	// 	$(this).hide();
	// })

	// $(".email_dialog .dialog").click(function(e) {
	// 	e.stopPropagation();
	// 	return false;
	// })

	$('.js-send-email').submit(function(e) {
		var $form = $(this);

		$(".email_dialog").find('.error_container').hide()

		// Disable the submit button to prevent repeated clicks
		$form.find('.js-send-btn').prop('disabled', true).text('Sending');

		$.ajax({
			type: "POST",
			url: "send.php",
			data: $form.serialize(),
			dataType: 'json',
			cache: false
		}).done(function(res) {

			if(res.status == 'ok')
			{
				$(".email_dialog").hide()
			}
			else
			{
				$(".email_dialog").find('.error_container').html(res.errors.join('<br>')).show()
			}

		}).always(function() {
			$form.find('.js-send-btn').prop('disabled', false).text('Send');
		})

		// Prevent the form from submitting with the default action
		return false;
	})
})
