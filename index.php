<?php

require_once("setup.php");
require_once('stripe-php/init.php');

$errors = array();

$total_transactions = db_query("SELECT SUM(`amount`) AS `total` FROM `transactions`");
$total_transactions = $total_transactions->fetchObject();

if(empty($total_transactions->total))
	$total_transactions->total = 0;

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['your_name']))
{
	if(empty($_POST['your_name']))
		$errors[] = 'Please enter your name';

	if(empty($_POST['your_email']))
		$errors[] = 'Please provide your email';

	if(!isset($_POST['amount']))
		$errors[] = 'Please select a donation amount';

	if(!isset($_POST['stripeToken']))
		$errors[] = 'Your credit card information seems to be invalid';

	// Everything looks good
	if(empty($errors))
	{
		$amount = intval($_POST['amount']);
		$token  = $_POST['stripeToken'];

		\Stripe\Stripe::setApiKey($config['stripe']['secret']);

		// Process Transaction
		try
		{
			$charge = \Stripe\Charge::create(array(
				'source'   => $token,
				'amount'   => $amount * 100, // Must be in cents
				'currency' => 'cad'
			));
		}
		catch(\Stripe\Error\Card $e)
		{
			// The card has been declined
			$body = $e->getJsonBody();
			$err  = $body['error'];

			$errors[] = $err['message'];
		}

		// one last check for errors
		if(empty($errors))
		{
			// Insert into DB
			db_query("INSERT INTO `transactions` (`name`, `email`, `amount`, `created_at`, `transaction_id`) VALUES (?, ?, ?, NOW(), ?)", $_POST['your_name'], $_POST['your_email'], $_POST['amount'], $charge->id);

			// Redirect to Thank you Page
			header("Location: thankyou.php");
			exit;
		}
	}
}

include("theme/header.php"); ?>

	<div id="header">

		<img src="links/logo.png" alt="Pneumaticity - Pet Rescue Fundraiser">
		<img src="links/pets.jpg" alt="Help the animals">

		<div class="progressBar">
			<p class="headerText">Help us raise funds for Kitty Kare, Northern Lights Dog Rescue and New Hope Dog Rescue. These organizations are committed to helping animals in need. With your help we want to help raise money for these three animal rescue organizations because every pet deserves a loving home.</p>
			<h2 class="barText">Amount raised to date <small>($<?php echo $total_transactions->total; ?>)</small></h2>

			<div class="progress_bar">
				<div class="goal">$<?php echo number_format($goal_amount); ?></div>
				<div class="bar<?php if($total_transactions->total / $goal_amount * 100 < 8) echo ' outside'; ?><?php if($total_transactions->total > 10) echo ' double'; ?>" style="width: <?php

					if($total_transactions->total >= $goal_amount)
						echo 100;
					else
						echo $total_transactions->total / $goal_amount * 100;

					?>%">
					<div class="current_amount"><?php

					if($total_transactions->total >= $goal_amount)
						echo 'Goal Achieved!';
					else
						echo '$' . $total_transactions->total;

					?></div>
				</div>
			</div>

			<div class="help">
				<h2 class="barText">Help spread the word</h2>
				<a href="#" class="button js-facebook-share">Share on your wall</a>
				<a href="#" class="button js-email-friend">Email a friend</a>
			</div>
		</div>

	</div>

	<div id="info">
		<h3 class="infoHead">To help realize our fundraising goal <strong>Pneumaticity Salon &amp; Spa</strong> will donate:</h3>

		<div class="infoTable">
			<div class="cell">
				<h4 class="amount"><sup>$</sup>1.00</h4>
				<p class="amountInfo">from every regular priced product sold.</p>
			</div>
			<div class="cell">
				<h4 class="amount">10<sup>%</sup></h4>
				<p class="amountInfo">from boots, hair tools, flat irons, curling irons, and blow dryers sales.</p>
			</div>
			<div class="cell">
				<h4 class="amount"><sup>$</sup>1.00</h4>
				<p class="amountInfo">from every new like on our Facebook page*.</p>
				<p class="disclaimer">up to a maximum of $100</p>
			</div>
		</div>
	</div>

	<div id="form">

		<form method="post" action="" id="mainForm">
			<h2 class="formHead">Help us raise money for Kitty Kare, Nothern Lights Dog Rescue and New hope Dog Rescue.</h2>

			<div class="errors"<?php echo !empty($errors) ? ' style="display: block;"' : ''; ?>>
				<?php if(!empty($errors)) : ?>
					<?php foreach($errors as $err) : ?>
						<div class="error"><?php echo $err; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<div class="field_row">
				<input type="text" placeholder="Your Name" name="your_name" value="<?php if(isset($_POST['your_name'])) echo $_POST['your_name']; ?>">
			</div>
			<div class="field_row">
				<input type="text" placeholder="Email Address" name="your_email" value="<?php if(isset($_POST['your_email'])) echo $_POST['your_email']; ?>">
			</div>
			<div class="field_row">
				<input type="text" placeholder="Name on Card" data-stripe="name">
			</div>
			<div class="field_row">
				<input type="text" placeholder="Card Number" data-stripe="number">
			</div>
			<div class="field_row">
				<input class="exp" size="2" maxlength="2" type="text" placeholder="Exp (MM)" data-stripe="exp-month">
				<input class="exp2" size="4" maxlength="4" type="text" placeholder="Exp (YYYY)" data-stripe="exp-year">
				<input class="cvv" size="4" maxlength="4" type="text" placeholder="CVV" data-stripe="cvc">
			</div>

			<div class="radios">
				<h2>Amount</h2>

				<input type="radio" name="amount" id="radio1" class="radio" value="5"<?php if(isset($_POST['amount']) && $_POST['amount'] == 5) echo ' checked="checked"'; ?>>
				<label for="radio1"><span></span></label>

				<input type="radio" name="amount" id="radio2" class="radio" value="10"<?php if(isset($_POST['amount']) && $_POST['amount'] == 10) echo ' checked="checked"'; ?>>
				<label for="radio2"><span></span></label>

				<input type="radio" name="amount" id="radio3" class="radio" value="15"<?php if(isset($_POST['amount']) && $_POST['amount'] == 15) echo ' checked="checked"'; ?>>
				<label for="radio3" ><span ></span></label>

				<input type="radio" name="amount" id="radio4" class="radio" value="20"<?php if(isset($_POST['amount']) && $_POST['amount'] == 20) echo ' checked="checked"'; ?>>
				<label for="radio4"><span></span></label>
			</div>

			<button class="submit" type="submit">Donate</button>
			<?php if(isset($_POST['stripeToken'])) : ?>
				<input type="hidden" name="stripeToken" class="stripeToken" value="<?php echo $_POST['stripeToken']; ?>">
			<?php endif; ?>
		</form>

	</div>

<?php include("theme/footer.php"); ?>