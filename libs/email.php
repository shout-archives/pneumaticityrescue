<?php
/**
 * Email
 *
 * @package Utopia
 * @author Shane Shipston <shaneshipston@gmail.com>
 * @version 0.1.0
 */
class Email
{
	/**
	 * Collection of recipients
	 *
	 * @since 0.1.0
	 * @var	array
	 */
	private $recipients = array();

	/**
	 * Collection of carbon copy recipients
	 *
	 * @since 0.1.0
	 * @var	array
	 */
	private $cc = array();

	/**
	 * Collection of blind carbon copy recipients
	 *
	 * @since 0.1.0
	 * @var	array
	 */
	private $bcc = array();

	/**
	 * Email subject
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $subject;

	/**
	 * Refined email message
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $message;

	/**
	 * Email body as submitted by user
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $raw_message;

	/**
	 * Alternate email body for non HTML clients
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $alt_message;

	/**
	 * From email
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $from;

	/**
	 * ReplyTo email
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $reply;

	/**
	 * Line breaks
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $libr = "\r\n";

	/**
	 * Priority
	 *
	 * @since 0.1.0
	 * @var	integer
	 */
	private $priority = 3;

	/**
	 * Body type
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $type = 'text/html';

	/**
	 * Character set
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $charset = 'iso-8859-1';

	/**
	 * Unique email identifier
	 *
	 * @since 0.1.0
	 * @var	string
	 */
	private $uid;

	/**
	 * Email headers
	 *
	 * @since 0.1.0
	 * @var	array
	 */
	private $headers = array();

	/**
	 * Attachments
	 *
	 * @since 0.1.0
	 * @var	array
	 */
	private $attachments = array();

	/**
	 * New Email
	 *
	 * @since 0.1.0
	 */
	public function __construct()
	{
		$this->create_mime_boundry();
	}

	/**
	 * Add Recipients
	 *
	 * @since 0.1.0
	 * @param string $email Email Address
	 * @param string $name Recipient Name
	 */
	public function add_recipient($email, $name = '')
	{
		$this->new_email($email, $name);

		return $this;
	}

	/**
	 * Add Recipients Alias
	 *
	 * @see add_recipient
	 */
	public function add_to($email, $name = '')
	{
		$this->add_recipient($email, $name);

		return $this;
	}

	/**
	 * Add Carbon Copies
	 *
	 * @since 0.1.0
	 * @param string $email Email Address
	 * @param string $name Recipient Name
	 */
	public function add_cc($email, $name = '')
	{
		$this->new_email($email, $name, 'cc');

		return $this;
	}

	/**
	 * Add Blind Carbon Copies
	 *
	 * @since 0.1.0
	 * @param string $email Email Address
	 * @param string $name Recipient Name
	 */
	public function add_bcc($email, $name = '')
	{
		$this->new_email($email, $name, 'bcc');

		return $this;
	}

	/**
	 * Modify Sender
	 *
	 * @since 0.1.0
	 * @param string $email From Email
	 * @param string $name Sender Name
	 */
	public function set_from($email, $name = '')
	{
		if($this->validate_email($email) && $this->dns_check($email))
		{
			if(!empty($name))
				$this->from = ucwords($name) . ' <' . $email . '>';
			else
				$this->from = $email;
		}

		return $this;
	}

	/**
	 * Set Reply
	 *
	 * @since 0.1.0
	 * @param string $email From Email
	 * @param string $name Sender Name
	 */
	public function set_reply($email, $name = '')
	{
		if($this->validate_email($email) && $this->dns_check($email))
		{
			if(!empty($name))
				$this->reply = ucwords($name) . ' <' . $email . '>';
			else
				$this->reply = $email;
		}

		return $this;
	}

	/**
	 * Set Subject
	 *
	 * @since 0.1.0
	 * @param string $subject Subject
	 */
	public function set_subject($subject)
	{
		$this->subject = trim($subject);

		return $this;
	}

	/**
	 * Set Email Body
	 *
	 * @since 0.1.0
	 * @param string $message Body
	 * @param string $type Content Type
	 * @param string $charset Character Set
	 */
	public function set_body($message, $type = '', $charset = '')
	{
		if(!empty($type))
			$this->type = $type;

		if(!empty($charset))
			$this->charset = $charset;

		$message = $this->strip_line_breaks($message);

		$this->raw_message = $message;

		return $this;
	}

	/**
	 * Set Alternate Body for Non HTML clients
	 *
	 * @since 0.1.0
	 * @param string $message Body
	 */
	public function set_alt_body($message)
	{
		$this->alt_message = $message;

		return $this;
	}

	/**
	 * Set Email Priority
	 *
	 * @since 0.1.0
	 * @param string|integer $priority New Priority
	 */
	public function set_priority($priority)
	{
		if(is_string($priority))
		{
			$priorities = array(
				'low'    => 4,
				'normal' => 3,
				'high'   => 2
			);

			if(array_key_exists($priority, $priorities))
				$this->priority = $priorities[$priority];
		}
		else
		{
			$this->priority = $priority;
		}

		return $this;
	}

	/**
	 * Add an Attachment
	 * 	- Disposition can be either attachment or inline (used for images inside HTML)
	 *
	 * @since 0.1.0
	 * @param string $filepath File Location
	 * @param string $disposition Content Disposition
	 */
	public function add_attachment($filepath, $disposition = 'attachment')
	{
		$file_info = $this->file_info($filepath);

		if(!$file_info)
			return;

		$filename = basename($filepath);
		$file_type = $this->mime_type($filepath);
		$chunks = chunk_split(base64_encode($file_info));

		$mail_part = '--' . $this->uid . $this->libr;
		$mail_part .= 'Content-type:' . $file_type . '; name="' . $filename . '"' . $this->libr;
		$mail_part .= 'Content-Transfer-Encoding: base64' . $this->libr;
		$mail_part .= 'Content-Disposition: ' . $disposition . ';' . $this->libr . $this->libr;
		$mail_part .= $chunks;
		$mail_part .= $this->libr . $this->libr;

		$this->attachments[] = $mail_part;

		return $this;
	}

	/**
	 * Process and Send the email
	 *
	 * @since 0.1.0
	 * @return boolean Success/Failure
	 */
	public function send()
	{
		if(empty($this->recipients))
			throw new Exception('No Recipients.');

		$headers = '';

		if(!empty($this->from))
		{
			$headers .= "From: " . $this->from . $this->libr;

			if(!empty($this->reply))
				$headers .= "Reply-To: " . $this->reply . $this->libr;
			else
				$headers .= "Reply-To: " . $this->from . $this->libr;
		}
		else
		{
			$headers .= "From: noreply@" . $this->domain_name() . $this->libr;
		}

		if(!empty($this->cc))
			$headers .= "Cc: ".implode(", ", $this->cc) . $this->libr;

		if(!empty($this->bcc))
			$headers .= "Bcc: ".implode(", ", $this->bcc) . $this->libr;

		$headers .= "MIME-Version: 1.0" . $this->libr;
		$headers .= "X-Priority: ".$this->priority . $this->libr;

		$this->message = $this->mail_body($this->raw_message);

		if(!empty($this->attachments)) // With Attachment
		{
			$headers .= 'Content-Type: multipart/mixed; boundary="' . $this->uid . '"' . $this->libr . $this->libr;

			$message = '--' . $this->uid . '' . $this->libr;
			$message .= 'Content-Type: multipart/alternative; boundary="mail_' . $this->uid . '"' . $this->libr . $this->libr;
			$message .= $this->message;

			foreach($this->attachments as $attach)
			{
				$message .= $attach;
			}

			$message .= '--' . $this->uid . '--';
		}
		elseif($this->type == 'text/html') // Without Attachment
		{
			$headers .= 'Content-Type: multipart/alternative; boundary="mail_' . $this->uid . '"' . $this->libr . $this->libr;
			$message = $this->message . $this->libr . $this->libr;
		}
		else
		{
			$message = $this->message . $this->libr . $this->libr;
		}

		if(empty($this->subject))
			$this->subject = '(No Subject)';

		return mail(implode(", ", $this->recipients), $this->subject, $message, $headers);
	}

	/**
	 * Ensure Proper Formatting on Emails
	 *
	 * @since 0.1.0
	 * @param string $email Email Address
	 * @return boolean Pass/Fail
	 */
	private function validate_email($email)
	{
		if(strpos($email, 'localhost') !== false)
			return true;

		if(filter_var($email, FILTER_VALIDATE_EMAIL))
			return true;

		return false;
	}

	/**
	 * Validate Email MX Records (Where possible)
	 *
	 * @since 0.1.0
	 * @param string $email Email Address
	 * @return boolean Pass/Fail
	 */
	private function dns_check($email)
	{
		if(strpos($email, 'localhost') !== false)
			return true;

		if(function_exists("checkdnsrr"))
		{
			$parts = explode("@", $email);

			if(checkdnsrr($parts[1], "MX"))
				return true;
			else
				return false;
		}

		return true;
	}

	/**
	 * Create a Unique Boundary
	 *
	 * @since 0.1.0
	 */
	private function create_mime_boundry()
	{
		$this->uid = md5(uniqid(time()));
	}

	/**
	 * Generate the email body
	 *
	 * @since 0.1.0
	 * @param string $message Unformatted Message
	 * @return string Formatted Email
	 */
	private function mail_body($message)
	{
		$mail_body = '--mail_' . $this->uid . '' . $this->libr;

		// Ensure plain text email clients can still read the email
		$mail_body .= 'Content-Type: text/plain; charset="' . $this->charset . '" ' . $this->libr;
		$mail_body .= 'Content-Transfer-Encoding: base64' . $this->libr . $this->libr;

		if(!empty($this->alt_message))
			$mail_body .= chunk_split(base64_encode(strip_tags(trim($this->alt_message)))) . $this->libr . $this->libr;
		else
			$mail_body .= chunk_split(base64_encode(strip_tags(trim($message)))) . $this->libr . $this->libr;

		$mail_body .= '--mail_' . $this->uid . '' . $this->libr;

		if($this->type == 'text/plain')
		{
			$mail_body .= $this->libr;
			return $mail_body;
		}

		$mail_body .= 'Content-Type: text/html; charset="' . $this->charset . '"' . $this->libr;
		$mail_body .= 'Content-Transfer-Encoding: base64' . $this->libr . $this->libr;
		$mail_body .= chunk_split(base64_encode(trim($message))) . $this->libr . $this->libr;

		$mail_body .= '--mail_' . $this->uid . '--' . $this->libr . $this->libr;

		return $mail_body;
	}

	/**
	 * Get File Information
	 *
	 * @since 0.1.0
	 * @param string $file File Location
	 * @return string|boolean File Contents/Fail
	 */
	private function file_info($file)
	{
		if(file_exists($file) && $info = file_get_contents($file))
			return $info;

		return false;
	}

	/**
	 * Add Email to respective pool
	 *
	 * @since 0.1.0
	 * @param string $email Email Address
	 * @param string $name Recipient Name
	 * @param string $type Pool Name
	 */
	private function new_email($email, $name, $type = 'recipients')
	{
		if($this->validate_email($email) && $this->dns_check($email))
		{
			// Format Email
			if(!empty($name))
				$recipient = ucwords($name) . ' <' . $email . '>';
			else
				$recipient = $email;

			// Add New Email to Pool
			switch($type)
			{
				case 'cc';
					if(!in_array($recipient, $this->cc))
						$this->cc[] = $recipient;
					break;
				case 'bcc';
					if(!in_array($recipient, $this->bcc))
						$this->bcc[] = $recipient;
					break;
				default:
					if(!in_array($recipient, $this->recipients))
						$this->recipients[] = $recipient;
					break;
			}
		}
	}

	/**
	 * Get MIME Type
	 *
	 * @since 0.1.0
	 * @param string $file File Location
	 * @return string MIME Type
	 */
	private function mime_type($file)
	{
		$path_parts = pathinfo($file);

		$mime_types = array(
			"pdf"  => "application/pdf",
			"exe"  => "application/octet-stream",
			"zip"  => "application/zip",
			"docx" => "application/msword",
			"doc"  => "application/msword",
			"xls"  => "application/vnd.ms-excel",
			"ppt"  => "application/vnd.ms-powerpoint",
			"gif"  => "image/gif",
			"png"  => "image/png",
			"jpeg" => "image/jpg",
			"jpg"  => "image/jpg",
			"mp3"  => "audio/mpeg",
			"wav"  => "audio/x-wav",
			"mpeg" => "video/mpeg",
			"mpg"  => "video/mpeg",
			"mpe"  => "video/mpeg",
			"mov"  => "video/quicktime",
			"avi"  => "video/x-msvideo",
			"3gp"  => "video/3gpp",
			"css"  => "text/css",
			"js"   => "application/javascript",
			"php"  => "text/html",
			"htm"  => "text/html",
			"html" => "text/html",
			"xml"  => "application/xml"
		);

		$extension = strtolower($path_parts['extension']);

		return $mime_types[$extension];
	}

	/**
	 * Remove Line Breaks
	 *
	 * @since 0.1.0
	 * @param string $message Content
	 * @return string Formatted Content
	 */
	private function strip_line_breaks($message)
	{
		return preg_replace("/([\r\n])/", "", $message);
	}

	/**
	 * Grab servers domain name
	 *
	 * @since 0.1.0
	 * @return string Formatted Content
	 */
	private function domain_name()
	{
		if(strpos($_SERVER['HTTP_HOST'], ':') !== false)
			$host = substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'], ':'));
		else
			$host = $_SERVER['HTTP_HOST'];

		return $host;
	}
}
