<?php

include("setup.php");

if(!detect_facebook() && !detect_mobile() && !empty($config['facebook_url']))
	header("Location: " . $config['facebook_url']);
else
	header("Location: " . $config['site_url']);