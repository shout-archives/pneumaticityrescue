</div>

	<script src="js/vendor/jquery-1.11.3.min.js"></script>
	<script type="text/javascript">
		// This identifies your website in the createToken call below
		Stripe.setPublishableKey("<?php echo $config['stripe']['publish']; ?>");

		var stripeResponseHandler = function(status, response) {
			var $form = $('#mainForm');

			if (response.error)
			{
				// Show the errors on the form
				$form.find('.errors').append('<div class="error">' + response.error.message + '</div>').show();
				$form.find('button').prop('disabled', false);
			}
			else
			{
				// token contains id, last4, and card type
				var token = response.id;

				// Insert the token into the form so it gets submitted to the server
				if($form.find('.stripeToken').length == 0)
					$form.append($('<input type="hidden" name="stripeToken" class="stripeToken" />').val(token));
				else
					$form.find('.stripeToken').val(token)

				// and re-submit
				$form.get(0).submit();
			}
		};

		jQuery(function($) {
			$('#mainForm').submit(function(e) {
				var $form = $(this);

				// Disable the submit button to prevent repeated clicks
				$form.find('button').prop('disabled', true);

				Stripe.card.createToken($form, stripeResponseHandler);

				// Prevent the form from submitting with the default action
				return false;
			});
		});
	</script>
	<script src="js/main.js"></script>
</body>
</html>