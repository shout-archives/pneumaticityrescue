<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Pneumaticity - Pet Rescue Fundraiser</title>
	<link rel="stylesheet" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<!-- The required Stripe lib -->
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	<!-- Facebook SDK -->
	<script src="//connect.facebook.net/en_US/sdk.js"></script>
</head>

<body>
	<div id="fb-root"></div>
	<script>
	window.fbAsyncInit = function() {
		// init the FB JS SDK
		FB.init({
			appId   : '<?php echo $config['app_id']; ?>', // App ID from the app dashboard
			status  : true, // check login status
			cookie  : true, // enable cookies to allow the server to access the session
			xfbml   : true, // parse XFBML
			version : 'v2.3'
		});

		FB.Canvas.setAutoGrow();
		FB.Canvas.scrollTo(0,0);
	};
	</script>

	<div class="email_dialog">
		<div class="dialog">
			<h2>Email a friend</h2>
			<div class="errors error_container"></div>
			<form method="post" action="send.php" class="js-send-email">
				<input type="text" name="your_email" placeholder="Your Email">
				<input type="text" name="friends_email" placeholder="Friend's email">
				<textarea name="your_message" placeholder="Your Message"></textarea>
				<button type="submit" class="js-send-btn">Send</button>
				<button type="button" class="js-cancel-email">Cancel</button>
			</form>
		</div>
	</div>

	<div id="wrapper">